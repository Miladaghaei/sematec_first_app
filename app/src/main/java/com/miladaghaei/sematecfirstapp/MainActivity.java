package com.miladaghaei.sematecfirstapp;


import android.content.Intent;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ActionMenuItemView;
import android.text.Editable;
import android.view.View;
import android.webkit.WebSettings;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.security.PublicKey;
import java.text.Format;
import java.text.Normalizer;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText editFname = (EditText) findViewById(R.id.edit_fname);
        final EditText editLname = (EditText) findViewById(R.id.edit_lname);
        final EditText editemail = (EditText) findViewById(R.id.edit_Email);
        final EditText editmobile = (EditText) findViewById(R.id.edit_Mobile);
        final Button buttonCreat = (Button) findViewById(R.id.button3);
        final Button buttonDel = (Button) findViewById(R.id.button2);
        buttonDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editFname.getText().clear();
                editLname.getText().clear();
                editemail.getText().clear();
                editmobile.getText().clear();
                Toast.makeText(getBaseContext(),"DELETE ALL",Toast.LENGTH_LONG).show();

            }
        });
        buttonCreat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Secondactivity.class);
                intent.putExtra("fname", editFname.getText().toString());
                intent.putExtra("lname", editLname.getText().toString());
                intent.putExtra("email", editemail.getText().toString());
                intent.putExtra("mobile", editmobile.getText().toString());

                startActivity(intent);
            }
        });
    }

}
