package com.miladaghaei.sematecfirstapp;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Secondactivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondactivity);
        TextView textView = (TextView) findViewById(R.id.textView);
        TextView textView1 = (TextView) findViewById(R.id.textView1);
        TextView textView2 = (TextView) findViewById(R.id.textView2);
        TextView textView3 = (TextView) findViewById(R.id.textView3);
        Intent intent = getIntent();
        String str = intent.getStringExtra("fname");
        String str1 = intent.getStringExtra("lname");
        String str2 = intent.getStringExtra("email");
        String str3 = intent.getStringExtra("mobile");
        textView.setText(str);
        textView1.setText(str1);
        textView2.setText(str2);
        textView3.setText(str3);
    }
}
